
# Введение

## Общие сведения

​	Настольные игры, как и многие другие развлечения, созданы для живого общения, для того, чтобы проводить время в компании людей, которые заинтересованы в том же, в чем и Вы. Однако далеко не каждый человек в наше время может быстро и удобно найти такую компанию, да и количество игр в разных жанрах поражает воображение. Так как же выбрать и найти людей с которыми совпадают Ваши вкусы? В эпоху развития технологий процесс поиска интересной компании для совместного времяпрепровождения так же должен быть современным. Данное web-приложение создано для поиска человека/компании, с целью поиграть в настольную игру и хорошо провести время. Для этого пользователи будут создавать соответствующие объявления или откликаться на уже созданные, знакомиться и играть. Основными критериями для поиска людей или компании будут: геолокация, возраст, пол, жанры игр.

## Цель и задачи

Цель - упростить процесс поиска людей для совместного времяпрепровождения за игрой в настольные игры.

Задачи:

* Создавать доски(объявления) с ограничивающими критериями(пол, возраст, город)
* Общаться с другими пользователями
* Изменять свой профиль
* Искать доски по своим предпочтениям

# Общее описание

## Роли пользователя

![](https://sun9-57.userapi.com/c857024/v857024389/b82eb/fopUM8U_3_Y.jpg)

## Интерфейсы пользователя

Главная страница

![](https://sun9-7.userapi.com/c858220/v858220737/b137a/TP-UEqOExq0.jpg)

![](https://sun9-4.userapi.com/c858220/v858220737/b13c0/whemP9Rw2eQ.jpg)

Настройки профиля
![](https://sun9-56.userapi.com/c858220/v858220737/b13a2/wOXoyj3YudY.jpg)

Авторизация и регистрация
![](https://sun9-17.userapi.com/c858132/v858132570/b44e3/iaHpV32DE58.jpg)

Свой профиль
![](https://vk.com/doc54546232_536565113?hash=36f71fbc19dee74592&dl=6e2eac6b8b54b116b3&wnd=1)

Чужой профиль
![](https://vk.com/doc54546232_536565105?hash=fe0d703f59f7fa59f9&dl=b58c08119901c9e32f&wnd=1)

Страница доски
![](https://sun9-13.userapi.com/c858220/v858220737/b1398/ea6-Ieqz_Hk.jpg)

Страница создания доски
![](https://sun9-52.userapi.com/c858220/v858220737/b13b6/ZZ5wQTgoKXE.jpg)

Чаты
![](https://sun9-64.userapi.com/c858220/v858220737/b1384/CZB6xNcWWi8.jpg)



## Интерфейсы программного обеспечения (Архитектура)

### Frontend

![](https://vk.com/doc54546232_536534758?hash=aa09266beb9f29afef&dl=e016024c27f934be13&wnd=1)

### Backend

![](https://vk.com/doc54546232_536534757?hash=9bb6ca602b1bdf79d0&dl=b0c3ae455e5c346f9f&wnd=1)

### Модель данных

![](https://vk.com/doc54546232_536534762?hash=773d6c8a5b7d2ba266&dl=bbd41e843691c3361f&wnd=1)

### Коммуникационные интерфейсы

Диаграмма взаимодействия Backend-Frontend

![](https://sun9-19.userapi.com/c858036/v858036271/bb01c/JNo0VbKeWF4.jpg)

![](https://sun9-45.userapi.com/c858036/v858036271/bb007/bFUl1hcJ9g4.jpg)

![](https://sun9-71.userapi.com/c858036/v858036271/bb015/4GktId5LAzg.jpg)

![](https://sun9-33.userapi.com/c858036/v858036271/bb00e/y1DoOCu9k-A.jpg)

#### Протокол взаимодействия Frontend и Backend

**_Главная_**

*Frontend:* GET /?gender="male/female/any"&age=18 40&city=Moscow

*Backend:* GET /api/desks?gender="male/female/any"&age=18 40&city=Moscow

**_Авторизация_**

*Frontend:* GET /signin

*Backend:* POST /api/signin

Тело:

```json
{
    "username": "text",
    "password": "text"
}
```

**_Регистрация_**

*Frontend:* GET /signup

*Backend:* POST /api/signup

Тело:

```json
{
    "username": "text",
    "email": "text",
    "password": "text",
    "birthDate": "YYYY-MM-DD",
    "gender": "int(0, 1)"
}
```


**_Профиль_**

*Frontend:* GET /profile/{username} 

*Backend:* GET /api/users/{username}/profile 

GET /api/users/id/desks?isOwner="true/false"

**_Профиль настройки_**

*Frontend:* GET /profile/{username}/settings

*Backend:* PUT /api/users/username/profile

Тело:

```json
 {
 	"newEmail": "text",
 	"newUsername": "text",
 	"newPassword": "text",
 	"newName": "text",
 	"newSurname": "text",
 	"newBirthDate": "YYYY-MM-DD",
 	"newGender": "int(0, 1)",
 	"newCity": "text",
 	"newAboutMe": "text",
 	"newFavoriteGenres": "{"text", "text"}",
 	"newFavoriteGames": "{"text", "text"}"
 }
```


**_Доска_**

*Frontend:* GET /desk/{id} 

*Backend:* GET /api/desks/{id} 

**_Доска создание_**

*Frontend:* GET /desk/new 

*Backend:* POST /api/desks

Тело:

```json
{
    "target": "",
    "title": "",
    "privacy": "",
    "gender": "",
    "ages": ["", ""],
    "city": "",
    "genres": [],
    "games": []
}
```

**_Сообщения_**

*Frontend:* GET /messages 

*Backend:* GET /api/users/id/chats?id= 

**_Чат_**

*Frontend:* GET /messages/chat_id 

*Backend:* GET /api/chat/id 

### Протокол взаимодействия по websocket

gm – получить все сообщения чата;

chats – чаты пользователя;

tm – новые сообщения в чатах с пользователями;

sm – отправить сообщение в чат с пользователем;

tdm – новые сообщения в чатах досок;

sdm – отправить сообщение в чат доски;

nu – новые уведомления.

## Технические детали реализации

Web-приложение должно быть развернуто в Docker-контейнерах. 

Верстка web-приложения не предусмотрена для мобильных устройств.  

Frontend: **ReactJS**

Backend: **NodeJS** + **fastify**

База данных: **PostgreSQL**

Раздача статики: **nginx**

## Требования к производительности

Персональный компьютер с установленным браузером и стабильным интернет подключением.

Доступные браузреы:  Google Chrome (Версия 80.0.3987.132)

Производительность backend: ? RPS

Производительность frontend: ? RPS

## Нефункциональные требования

* Доступ на API по JWT токену
* Шифрование паролей пользователей

